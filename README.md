# Página Web Digiartes 2017

Esta página web está construida para la Agencia Web [Digiartes](https://digiartes.com/).

Está construida utilizando Nuxtjs. La página web es generada en el cliente y hospedada estáticamente.

Clona este repositorio a tu computadora y ejecuta el comando *npm install* para instalar todas las dependecias.

Luego de esto ejecuta el comando *npm start* para iniciar la aplicación. La aplicación escuchará los cambios en los archivos y directorios para refrescar el navegador.

Si necesitas generar los archivos nuevamente ejecuta *npm generate* y sube los archivos dentro de static a cualquier servidor web.