module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Digiartes | Tu Agencia Web',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Digiartes Website' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Inconsolata|Ubuntu|Ubuntu+Condensed' }
    ],
  },
  css: [
    '~/assets/sass/main.sass'
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#0D0086' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run Axios
    */
    vendor: ['axios'],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
